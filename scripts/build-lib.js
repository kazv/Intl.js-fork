/*global Promise*/

import * as p from 'path';
import * as fs from 'fs';
import {rollup} from 'rollup';
import babel from '@rollup/plugin-babel';

// let babelConfig = JSON.parse(fs.readFileSync('src/.babelrc', 'utf8'));
// babelConfig.babelrc = false;
// babelConfig.presets = babelConfig.presets.map((preset) => {
//   //return preset === 'es2015' ? 'es2015-rollup' : preset;
//   return preset;
// });
const babelConfig = {};

let bundle = rollup({
    input: p.resolve('src/core.js'),
    plugins: [
        babel(babelConfig)
    ]
});

// Cast to native Promise.
bundle = Promise.resolve(bundle);

bundle.then(({write}) => write({
    file: p.resolve('lib/core.js'),
  format: 'cjs',
  exports: 'auto'
}));

process.on('unhandledRejection', (reason) => {throw reason;});
